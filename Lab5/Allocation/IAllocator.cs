﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Allocation
{
    public interface IAllocator
    {
        IEnumerable<Node> Nodes { get; }
    }
}
