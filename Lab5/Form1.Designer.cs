﻿namespace Lab5
{
	partial class MemoryAllocator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.DumpBox = new System.Windows.Forms.RichTextBox();
            this.DumpLabel = new System.Windows.Forms.Label();
            this.CapacityGroup = new System.Windows.Forms.GroupBox();
            this.capacity6 = new System.Windows.Forms.RadioButton();
            this.AllocateButton = new System.Windows.Forms.Button();
            this.capacity5 = new System.Windows.Forms.RadioButton();
            this.capacity4 = new System.Windows.Forms.RadioButton();
            this.capacity3 = new System.Windows.Forms.RadioButton();
            this.capacity2 = new System.Windows.Forms.RadioButton();
            this.capacity1 = new System.Windows.Forms.RadioButton();
            this.TaskGroup = new System.Windows.Forms.GroupBox();
            this.AddTaskButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TaskSize = new System.Windows.Forms.NumericUpDown();
            this.SegmentGroup = new System.Windows.Forms.GroupBox();
            this.SelectButton = new System.Windows.Forms.Button();
            this.SegmentSise5 = new System.Windows.Forms.RadioButton();
            this.SegmentSise4 = new System.Windows.Forms.RadioButton();
            this.SegmentSise3 = new System.Windows.Forms.RadioButton();
            this.SegmentSise2 = new System.Windows.Forms.RadioButton();
            this.SegmentSise1 = new System.Windows.Forms.RadioButton();
            this.TaskList = new System.Windows.Forms.ListBox();
            this.NodeList = new System.Windows.Forms.ListBox();
            this.RemoveTaskButton = new System.Windows.Forms.Button();
            this.CopyByteButton = new System.Windows.Forms.Button();
            this.FromNumeric = new System.Windows.Forms.NumericUpDown();
            this.ToNumeric = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ChangeGroup = new System.Windows.Forms.GroupBox();
            this.CapacityGroup.SuspendLayout();
            this.TaskGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaskSize)).BeginInit();
            this.SegmentGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FromNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToNumeric)).BeginInit();
            this.ChangeGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // DumpBox
            // 
            this.DumpBox.Location = new System.Drawing.Point(12, 25);
            this.DumpBox.Name = "DumpBox";
            this.DumpBox.ReadOnly = true;
            this.DumpBox.Size = new System.Drawing.Size(987, 329);
            this.DumpBox.TabIndex = 0;
            this.DumpBox.Text = "";
            // 
            // DumpLabel
            // 
            this.DumpLabel.AutoSize = true;
            this.DumpLabel.Location = new System.Drawing.Point(12, 9);
            this.DumpLabel.Name = "DumpLabel";
            this.DumpLabel.Size = new System.Drawing.Size(75, 13);
            this.DumpLabel.TabIndex = 1;
            this.DumpLabel.Text = "Memory Dump";
            // 
            // CapacityGroup
            // 
            this.CapacityGroup.Controls.Add(this.capacity6);
            this.CapacityGroup.Controls.Add(this.AllocateButton);
            this.CapacityGroup.Controls.Add(this.capacity5);
            this.CapacityGroup.Controls.Add(this.capacity4);
            this.CapacityGroup.Controls.Add(this.capacity3);
            this.CapacityGroup.Controls.Add(this.capacity2);
            this.CapacityGroup.Controls.Add(this.capacity1);
            this.CapacityGroup.Enabled = false;
            this.CapacityGroup.Location = new System.Drawing.Point(173, 362);
            this.CapacityGroup.Name = "CapacityGroup";
            this.CapacityGroup.Size = new System.Drawing.Size(107, 197);
            this.CapacityGroup.TabIndex = 2;
            this.CapacityGroup.TabStop = false;
            this.CapacityGroup.Text = "Capacity";
            // 
            // capacity6
            // 
            this.capacity6.AutoSize = true;
            this.capacity6.Location = new System.Drawing.Point(7, 140);
            this.capacity6.Name = "capacity6";
            this.capacity6.Size = new System.Drawing.Size(59, 17);
            this.capacity6.TabIndex = 5;
            this.capacity6.Text = "512 kB";
            this.capacity6.UseVisualStyleBackColor = true;
            // 
            // AllocateButton
            // 
            this.AllocateButton.Location = new System.Drawing.Point(6, 168);
            this.AllocateButton.Name = "AllocateButton";
            this.AllocateButton.Size = new System.Drawing.Size(75, 23);
            this.AllocateButton.TabIndex = 6;
            this.AllocateButton.Text = "Allocate";
            this.AllocateButton.UseVisualStyleBackColor = true;
            this.AllocateButton.Click += new System.EventHandler(this.AllocateButton_Click);
            // 
            // capacity5
            // 
            this.capacity5.AutoSize = true;
            this.capacity5.Location = new System.Drawing.Point(7, 116);
            this.capacity5.Name = "capacity5";
            this.capacity5.Size = new System.Drawing.Size(59, 17);
            this.capacity5.TabIndex = 4;
            this.capacity5.Text = "256 kB";
            this.capacity5.UseVisualStyleBackColor = true;
            // 
            // capacity4
            // 
            this.capacity4.AutoSize = true;
            this.capacity4.Location = new System.Drawing.Point(7, 92);
            this.capacity4.Name = "capacity4";
            this.capacity4.Size = new System.Drawing.Size(59, 17);
            this.capacity4.TabIndex = 3;
            this.capacity4.Text = "128 kB";
            this.capacity4.UseVisualStyleBackColor = true;
            // 
            // capacity3
            // 
            this.capacity3.AutoSize = true;
            this.capacity3.Location = new System.Drawing.Point(7, 68);
            this.capacity3.Name = "capacity3";
            this.capacity3.Size = new System.Drawing.Size(53, 17);
            this.capacity3.TabIndex = 2;
            this.capacity3.Text = "64 kB";
            this.capacity3.UseVisualStyleBackColor = true;
            // 
            // capacity2
            // 
            this.capacity2.AutoSize = true;
            this.capacity2.Location = new System.Drawing.Point(7, 44);
            this.capacity2.Name = "capacity2";
            this.capacity2.Size = new System.Drawing.Size(53, 17);
            this.capacity2.TabIndex = 1;
            this.capacity2.Text = "32 kB";
            this.capacity2.UseVisualStyleBackColor = true;
            // 
            // capacity1
            // 
            this.capacity1.AutoSize = true;
            this.capacity1.Checked = true;
            this.capacity1.Location = new System.Drawing.Point(7, 20);
            this.capacity1.Name = "capacity1";
            this.capacity1.Size = new System.Drawing.Size(53, 17);
            this.capacity1.TabIndex = 0;
            this.capacity1.TabStop = true;
            this.capacity1.Text = "16 kB";
            this.capacity1.UseVisualStyleBackColor = true;
            // 
            // TaskGroup
            // 
            this.TaskGroup.Controls.Add(this.AddTaskButton);
            this.TaskGroup.Controls.Add(this.label1);
            this.TaskGroup.Controls.Add(this.TaskSize);
            this.TaskGroup.Enabled = false;
            this.TaskGroup.Location = new System.Drawing.Point(286, 362);
            this.TaskGroup.Name = "TaskGroup";
            this.TaskGroup.Size = new System.Drawing.Size(118, 197);
            this.TaskGroup.TabIndex = 3;
            this.TaskGroup.TabStop = false;
            this.TaskGroup.Text = "Task";
            // 
            // AddTaskButton
            // 
            this.AddTaskButton.Location = new System.Drawing.Point(6, 168);
            this.AddTaskButton.Name = "AddTaskButton";
            this.AddTaskButton.Size = new System.Drawing.Size(75, 23);
            this.AddTaskButton.TabIndex = 2;
            this.AddTaskButton.Text = "AddTask";
            this.AddTaskButton.UseVisualStyleBackColor = true;
            this.AddTaskButton.Click += new System.EventHandler(this.AddTaskButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Bytes";
            // 
            // TaskSize
            // 
            this.TaskSize.Increment = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.TaskSize.Location = new System.Drawing.Point(7, 20);
            this.TaskSize.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.TaskSize.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.TaskSize.Name = "TaskSize";
            this.TaskSize.Size = new System.Drawing.Size(68, 20);
            this.TaskSize.TabIndex = 0;
            this.TaskSize.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // SegmentGroup
            // 
            this.SegmentGroup.Controls.Add(this.SelectButton);
            this.SegmentGroup.Controls.Add(this.SegmentSise5);
            this.SegmentGroup.Controls.Add(this.SegmentSise4);
            this.SegmentGroup.Controls.Add(this.SegmentSise3);
            this.SegmentGroup.Controls.Add(this.SegmentSise2);
            this.SegmentGroup.Controls.Add(this.SegmentSise1);
            this.SegmentGroup.Location = new System.Drawing.Point(12, 362);
            this.SegmentGroup.Name = "SegmentGroup";
            this.SegmentGroup.Size = new System.Drawing.Size(155, 197);
            this.SegmentGroup.TabIndex = 4;
            this.SegmentGroup.TabStop = false;
            this.SegmentGroup.Text = "Segment";
            // 
            // SelectButton
            // 
            this.SelectButton.Location = new System.Drawing.Point(6, 168);
            this.SelectButton.Name = "SelectButton";
            this.SelectButton.Size = new System.Drawing.Size(75, 23);
            this.SelectButton.TabIndex = 6;
            this.SelectButton.Text = "Select";
            this.SelectButton.UseVisualStyleBackColor = true;
            this.SelectButton.Click += new System.EventHandler(this.SelectButton_Click);
            // 
            // SegmentSise5
            // 
            this.SegmentSise5.AutoSize = true;
            this.SegmentSise5.Location = new System.Drawing.Point(7, 118);
            this.SegmentSise5.Name = "SegmentSise5";
            this.SegmentSise5.Size = new System.Drawing.Size(66, 17);
            this.SegmentSise5.TabIndex = 4;
            this.SegmentSise5.Text = "32 Bytes";
            this.SegmentSise5.UseVisualStyleBackColor = true;
            // 
            // SegmentSise4
            // 
            this.SegmentSise4.AutoSize = true;
            this.SegmentSise4.Location = new System.Drawing.Point(7, 94);
            this.SegmentSise4.Name = "SegmentSise4";
            this.SegmentSise4.Size = new System.Drawing.Size(66, 17);
            this.SegmentSise4.TabIndex = 3;
            this.SegmentSise4.Text = "16 Bytes";
            this.SegmentSise4.UseVisualStyleBackColor = true;
            // 
            // SegmentSise3
            // 
            this.SegmentSise3.AutoSize = true;
            this.SegmentSise3.Checked = true;
            this.SegmentSise3.Location = new System.Drawing.Point(7, 70);
            this.SegmentSise3.Name = "SegmentSise3";
            this.SegmentSise3.Size = new System.Drawing.Size(60, 17);
            this.SegmentSise3.TabIndex = 2;
            this.SegmentSise3.TabStop = true;
            this.SegmentSise3.Text = "8 Bytes";
            this.SegmentSise3.UseVisualStyleBackColor = true;
            // 
            // SegmentSise2
            // 
            this.SegmentSise2.AutoSize = true;
            this.SegmentSise2.Location = new System.Drawing.Point(7, 46);
            this.SegmentSise2.Name = "SegmentSise2";
            this.SegmentSise2.Size = new System.Drawing.Size(60, 17);
            this.SegmentSise2.TabIndex = 1;
            this.SegmentSise2.Text = "4 Bytes";
            this.SegmentSise2.UseVisualStyleBackColor = true;
            // 
            // SegmentSise1
            // 
            this.SegmentSise1.AutoSize = true;
            this.SegmentSise1.Location = new System.Drawing.Point(7, 22);
            this.SegmentSise1.Name = "SegmentSise1";
            this.SegmentSise1.Size = new System.Drawing.Size(60, 17);
            this.SegmentSise1.TabIndex = 0;
            this.SegmentSise1.Text = "2 Bytes";
            this.SegmentSise1.UseVisualStyleBackColor = true;
            // 
            // TaskList
            // 
            this.TaskList.FormattingEnabled = true;
            this.TaskList.Location = new System.Drawing.Point(410, 360);
            this.TaskList.Name = "TaskList";
            this.TaskList.Size = new System.Drawing.Size(132, 160);
            this.TaskList.TabIndex = 5;
            this.TaskList.SelectedIndexChanged += new System.EventHandler(this.TaskList_SelectedIndexChanged);
            // 
            // NodeList
            // 
            this.NodeList.FormattingEnabled = true;
            this.NodeList.Location = new System.Drawing.Point(689, 360);
            this.NodeList.Name = "NodeList";
            this.NodeList.Size = new System.Drawing.Size(234, 199);
            this.NodeList.TabIndex = 6;
            // 
            // RemoveTaskButton
            // 
            this.RemoveTaskButton.Enabled = false;
            this.RemoveTaskButton.Location = new System.Drawing.Point(410, 530);
            this.RemoveTaskButton.Name = "RemoveTaskButton";
            this.RemoveTaskButton.Size = new System.Drawing.Size(132, 23);
            this.RemoveTaskButton.TabIndex = 3;
            this.RemoveTaskButton.Text = "RemoveTask";
            this.RemoveTaskButton.UseVisualStyleBackColor = true;
            this.RemoveTaskButton.Click += new System.EventHandler(this.RemoveTaskButton_Click);
            // 
            // CopyByteButton
            // 
            this.CopyByteButton.Location = new System.Drawing.Point(6, 168);
            this.CopyByteButton.Name = "CopyByteButton";
            this.CopyByteButton.Size = new System.Drawing.Size(110, 23);
            this.CopyByteButton.TabIndex = 7;
            this.CopyByteButton.Text = "CopyByte";
            this.CopyByteButton.UseVisualStyleBackColor = true;
            this.CopyByteButton.Click += new System.EventHandler(this.CopyByteButton_Click);
            // 
            // FromNumeric
            // 
            this.FromNumeric.Location = new System.Drawing.Point(6, 115);
            this.FromNumeric.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.FromNumeric.Name = "FromNumeric";
            this.FromNumeric.Size = new System.Drawing.Size(79, 20);
            this.FromNumeric.TabIndex = 8;
            // 
            // ToNumeric
            // 
            this.ToNumeric.Location = new System.Drawing.Point(7, 137);
            this.ToNumeric.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.ToNumeric.Name = "ToNumeric";
            this.ToNumeric.Size = new System.Drawing.Size(78, 20);
            this.ToNumeric.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(92, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "From";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(92, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "To";
            // 
            // ChangeGroup
            // 
            this.ChangeGroup.Controls.Add(this.CopyByteButton);
            this.ChangeGroup.Controls.Add(this.label3);
            this.ChangeGroup.Controls.Add(this.FromNumeric);
            this.ChangeGroup.Controls.Add(this.label2);
            this.ChangeGroup.Controls.Add(this.ToNumeric);
            this.ChangeGroup.Enabled = false;
            this.ChangeGroup.Location = new System.Drawing.Point(549, 362);
            this.ChangeGroup.Name = "ChangeGroup";
            this.ChangeGroup.Size = new System.Drawing.Size(134, 197);
            this.ChangeGroup.TabIndex = 12;
            this.ChangeGroup.TabStop = false;
            this.ChangeGroup.Text = "Changes";
            // 
            // MemoryAllocator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 571);
            this.Controls.Add(this.ChangeGroup);
            this.Controls.Add(this.RemoveTaskButton);
            this.Controls.Add(this.NodeList);
            this.Controls.Add(this.TaskList);
            this.Controls.Add(this.SegmentGroup);
            this.Controls.Add(this.TaskGroup);
            this.Controls.Add(this.CapacityGroup);
            this.Controls.Add(this.DumpLabel);
            this.Controls.Add(this.DumpBox);
            this.Name = "MemoryAllocator";
            this.Text = "Form1";
            this.CapacityGroup.ResumeLayout(false);
            this.CapacityGroup.PerformLayout();
            this.TaskGroup.ResumeLayout(false);
            this.TaskGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaskSize)).EndInit();
            this.SegmentGroup.ResumeLayout(false);
            this.SegmentGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FromNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToNumeric)).EndInit();
            this.ChangeGroup.ResumeLayout(false);
            this.ChangeGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        #endregion

        private System.Windows.Forms.RichTextBox DumpBox;
        private System.Windows.Forms.Label DumpLabel;
        private System.Windows.Forms.GroupBox CapacityGroup;
        private System.Windows.Forms.RadioButton capacity6;
        private System.Windows.Forms.RadioButton capacity5;
        private System.Windows.Forms.RadioButton capacity4;
        private System.Windows.Forms.RadioButton capacity3;
        private System.Windows.Forms.RadioButton capacity2;
        private System.Windows.Forms.RadioButton capacity1;
        private System.Windows.Forms.Button AllocateButton;
        private System.Windows.Forms.GroupBox TaskGroup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown TaskSize;
        private System.Windows.Forms.Button AddTaskButton;
        private System.Windows.Forms.GroupBox SegmentGroup;
        private System.Windows.Forms.RadioButton SegmentSise5;
        private System.Windows.Forms.RadioButton SegmentSise4;
        private System.Windows.Forms.RadioButton SegmentSise3;
        private System.Windows.Forms.RadioButton SegmentSise2;
        private System.Windows.Forms.RadioButton SegmentSise1;
        private System.Windows.Forms.Button SelectButton;
        private System.Windows.Forms.ListBox TaskList;
        private System.Windows.Forms.ListBox NodeList;
        private System.Windows.Forms.Button RemoveTaskButton;
        private System.Windows.Forms.Button CopyByteButton;
        private System.Windows.Forms.NumericUpDown FromNumeric;
        private System.Windows.Forms.NumericUpDown ToNumeric;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox ChangeGroup;
    }
}

