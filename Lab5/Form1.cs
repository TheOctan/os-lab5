﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab5.Allocation;
using Lab5.Memory;

// 1.2 Организация простой многозадачности с помощью смещения адресов
// 
// > обозначение адреса для каждого сегмента памяти,
// > выбор размера оперативной памяти(от 16 Кб до 512 Кб),
// > выбор размера загружаемого процесса,
// > загрузка процесса в оперативную память,
// > выгрузка процесса из оперативной памяти,
// > смещение процесса в оперативной памяти,
// > отображение величины смещения,
// > обращение из какой либо ячейки памяти(занятой процессом)
// к любой другой(занятой тем же процессом), визуализировать данные адреса обращения.
//
// Реализовать возможность загрузки в оперативную память 3-х и более процессов.

// 2.2 Управление памятью с помощью связных списков
//
// > выбор размера сегмента памяти(от 1 Кб до 4 Кб),
// > визуализация связного списка, а так же его размера,
// > организация поиска свободного пространства памяти.

namespace Lab5
{
    public partial class MemoryAllocator : Form
    {
        Manager manager = new Manager();

        public MemoryAllocator()
        {
            InitializeComponent();
        }

        private void InitializeDump()
        {
            DumpBox.Text = DumpToString();
            DumpLabel.Text = string.Format("Memory Dump {0} kB", manager.Segmenter.Capacity);
            UpdateNodeList();
            UpdateTaskList();
            RemoveTaskButton.Enabled = false;
            ChangeGroup.Enabled = false;
        }

        private string DumpToString()
        {
            StringBuilder builder = new StringBuilder();

            foreach (var segment in manager.Segmenter.Segments)
            {
                builder.Append(segment.ToString()).Append('\n');
            }

            return builder.ToString();
        }

        private byte[] ToBytes(int[] mas)
        {
            return mas.Select(x => (byte)x).ToArray();
        }
        private byte[] ToBytes(float[] mas)
        {
            return mas.Select(x => (byte)x).ToArray();
        }
        private byte[] ToBytes(char[] mas)
        {
            return mas.Select(x => (byte)x).ToArray();
        }


        private void AllocateButton_Click(object sender, EventArgs e)
        {
            if (capacity1.Checked) { manager.ResetMemory(16); }
            else if (capacity2.Checked) { manager.ResetMemory(32); }
            else if (capacity3.Checked) { manager.ResetMemory(64); }
            else if (capacity4.Checked) { manager.ResetMemory(128); }
            else if (capacity5.Checked) { manager.ResetMemory(256); }
            else if (capacity6.Checked) { manager.ResetMemory(512); }

            InitializeDump();

            SegmentGroup.Enabled = true;
            CapacityGroup.Enabled = false;
        }

        private void SelectButton_Click(object sender, EventArgs e)
        {
            if (SegmentSise1.Checked) Segment.size = 2;
            else if (SegmentSise2.Checked) Segment.size = 4;
            else if (SegmentSise3.Checked) Segment.size = 8;
            else if (SegmentSise4.Checked) Segment.size = 16;
            else if (SegmentSise5.Checked) Segment.size = 32;

            SegmentGroup.Enabled = false;
            CapacityGroup.Enabled = true;
            TaskGroup.Enabled = true;
        }

        private void AddTaskButton_Click(object sender, EventArgs e)
        {
            if (manager.AddTask((int)TaskSize.Value))
            {
                InitializeDump();
                UpdateTaskList();
                UpdateNodeList();
            }
        }

        private void UpdateNodeList()
        {
            NodeList.Items.Clear();

            foreach (var node in manager.Allocator.Nodes.Reverse())
            {
                NodeList.Items.Add(string.Format("{0}\t{1}\t{2} segments", node.IsFree, Segment.AddressFormat(node.Address), node.CountSegments));
            }
        }

        private void UpdateTaskList()
        {
            TaskList.Items.Clear();

            foreach (var task in manager.Tasks.Reverse())
            {
                TaskList.Items.Add(task);
            }
        }

        private void TaskList_SelectedIndexChanged(object sender, EventArgs e)
        {
            RemoveTaskButton.Enabled = true;
            ChangeGroup.Enabled = true;
        }

        private void RemoveTaskButton_Click(object sender, EventArgs e)
        {
            int index = manager.Tasks.Count() - TaskList.SelectedIndex - 1;

            if (manager.RemoveTask(index))
            {
                InitializeDump();
            }
        }

        private void CopyByteButton_Click(object sender, EventArgs e)
        {
            int index = manager.Tasks.Count() - TaskList.SelectedIndex - 1;

            if(manager.CloneByte(index, (uint)FromNumeric.Value, (uint)ToNumeric.Value))
            {
                InitializeDump();
            }
        }
    }
}
